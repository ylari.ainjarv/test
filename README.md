Kirjutada programm, mis viskab (näiteks: java.util.Random) kaht täringut ja näitab tulemust pseudograafiliselt
ekraanil (standardväljund) koos saavutatud silmade arvuga (kahe täringu summa):

```
+-------+ +-------+
|       | | *     |
| *   * | |   *   | = 5
|       | |     * |
+-------+ +-------+
```
```
+-------+
|       |
|   *   |
|       |
+-------+
+-------+
|       |
| *   * |
|       |
+-------+
+-------+
| *     |
|   *   |
|     * |
+-------+
+-------+
| *   * |
|       |
| *   * |
+-------+
+-------+
| *   * |
|   *   |
| *   * |
+-------+
+-------+
| *   * |
| *   * |
| *   * |
+-------+
```
 Oma lahendus (.java) saada mulle (meiliga, Slacki, GitLabi ...).
 Soovin kõigile edu. Ülari
